
let tabContainer = document.getElementById("tabs");
let tabsTitle = tabContainer.children;

let tabContent  = document.getElementById("tabs-content");
let tabsText = tabContent.children;

for (let item of [...tabsText])
{
    item.hidden = true;
}

tabContainer.addEventListener("click", evt => {
    const pressed=evt.target;
    let index = [...tabsTitle].indexOf(pressed);

    for(let i = 0;i < tabsTitle.length;i++)
    {
        if (i===index)
        {
            tabsText[i].hidden=false;
            tabsTitle[i].classList.add("active")

        }
        else
        {
            tabsText[i].hidden=true;
            tabsTitle[i].classList.remove("active");
        }
    }
});
